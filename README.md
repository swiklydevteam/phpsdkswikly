# Instructions

1 - Download the file 'phpSdkSwikly.php' from bitbucket.

2 - Copy it in your project for example: includes/phpSdkSwikly.php

3 - Require the SDK in your php file:
```PHP
<?php
// 1. Load the SDK file.
require __DIR__  . '/includes/phpSdkSwikly.php';
```

4 - Provide your API key, API secret and the environment you want to work in (development or production) to the Swikly API object:
```PHP
<?php
$swkAPI = new \Swikly\SwiklyAPI('example_api_key', 'API_SECRET', 'development');
```

5 - Create a swik and fill it with all your informations:
```PHP
<?php
// Create a swik object
$swik = new \Swikly\Swik();

// Set all the swik informations
$swik->setClientFirstName("Jean")
    ->setClientLastName("Dupont")
    ->setClientEmail("jean.dupont@gmail.com")
    ->setClientPhoneNumber("+33 6 20 20 20 20") // Send SMS to that number
    ->setClientLanguage("FR") // "EN", "FR", 'NL', 'DE'
    ->setSwikAmount("50")
    ->setSwikDescription("1h de canyoning le 12/08/2017....")
    ->setSwikEndDay("12")
    ->setSwikEndMonth("08")
    ->setSwikEndYear("2017")
    ->setSwikId("YOUR_ID")
    ->setSendEmail("true")
    ->setSwikType("security deposit") // "reservation" or "security deposit"
    ->setCallbackUrl('https://mywebsite.com/resultSwik');

// Create and send your new swik to your client
$result = $swkAPI->newSwik($swik);

// Print result of the operation
if ($result['status'] == 'ok') {
	echo "New swik created\n";
    echo "Your client can accept the swik at that address: " . $result['acceptUrl'];
} else {
	echo "Failed create swik";
	echo "Error = " . $result['message'];
}
```

6 - Create a payment:
```PHP
<?php
// Create a swik object
$swik = new \Swikly\Swik();

// Set all the swik informations
$swik->setClientFirstName("Jean")
    ->setClientLastName("Dupont")
    ->setClientEmail("jean.dupont@gmail.com")
    ->setClientPhoneNumber("+33 6 20 20 20 20") // Send SMS to that number
    ->setClientLanguage("FR") // "EN", "FR", 'NL', 'DE'
    ->setSwikAmount("50")
    ->setSwikDescription("1h de canyoning le 12/08/2017....")
    ->setSwikId("YOUR_ID")
    ->setSendEmail("true")
    ->setCallbackUrl('https://mywebsite.com/resultSwik');

// Create and send your new swik to your client
$result = $swkAPI->newPayment($swik);

// Print result of the operation
if ($result['status'] == 'ok') {
    echo "New payment created\n";
    echo "Your client can pay you at that address: " . $result['acceptUrl'];
} else {
    echo "Failed create swik";
    echo "Error = " . $result['message'];
}
```

7 - Canceling a swik:
```PHP
<?php

// Create a swik object
$swik = new \Swikly\Swik();

// Set the Id you used to create it
$swik->setSwikId("YOUR_ID");

// Deleting the swik
$result = $swkAPI->deleteSwik($swik);

// Print result of the operation
if ($result['status'] == 'ok') {
    echo "Swik deleted correctly";
} else {
    echo "Failed delete swik";
    echo "Error = " . $result['message'];
}
```

8 - Getting my list of swiks:
```PHP
<?php

// Get the list of your swiks
$result = $swkAPI->getListSwik();

// Print result of the operation
if ($result['status'] == 'ok') {
    echo "List of swik(s) = ";
    print_r($result['list']);
} else {
    echo "Failed getting the swik list";
    echo "Error = " . $result['message'];
}
```

9 - Getting a specific Swik:
```PHP
<?php

// Create a swik object
$swik = new \Swikly\Swik();

// Set the Id you used to create it
$swik->setSwikId("YOUR_ID");

// Get the list of your swiks
$result = $swkAPI->getSwik($swik);

// Print result of the operation
if ($result['status'] == 'ok') {
    echo "My swik = ";
    print_r($result['swik']);
} else {
    echo "Failed getting the swik list";
    echo "Error = " . $result['message'];
}
```

10 - Direct Swik

Direct swik are used to integrate [Swikly](https://www.swikly.com/) directly to your website.

For more informations look at the [Wiki](https://bitbucket.org/swiklydevteam/phpsdkswikly/wiki/Make_a_Direct_Swik).

Examples are available in the examples directory of this repository.