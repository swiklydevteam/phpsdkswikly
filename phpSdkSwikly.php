<?php

namespace Swikly;

/**
 **
 ** The Swik class let you create and manage Swiks.
 ** Simple class with setter and getter.
 **
 */
class Swik
{
	private $clientFirstName;
	private $clientLastName;
	private $clientPhoneNumber;
	private $clientEmail;
	private $clientLanguage;
	private $sendEmail;
	private $swikAmount;
	private $swikDescription;
	private $swikEndDay;
	private $swikEndMonth;
	private $swikEndYear;
	private $swikId;
	private $swikType;
	private $callbackUrl;
	private $businessPays;

    //All the setters
	public function setClientFirstName($firstName) {
		$this->clientFirstName = $firstName;
		return $this;
	}

    public function setClientLastName($lastName) {
    	$this->clientLastName = $lastName;
    	return $this;
    }

    public function setClientPhoneNumber($phoneNumber) {
    	$this->clientPhoneNumber = $phoneNumber;
    	return $this;
    }

    public function setClientEmail($email) {
    	$this->clientEmail = $email;
    	return $this;
    }

    public function setClientLanguage($lang) {
    	$this->clientLanguage = $lang;
    	return $this;
	}

    public function setSendEmail($sendEmail) {
    	$this->sendEmail = $sendEmail;
    	return $this;
    }

    public function setSwikAmount($amount) {
    	$this->swikAmount = $amount;
    	return $this;
    }

    public function setSwikDescription($desc) {
    	$this->swikDescription = $desc;
    	return $this;
    }

    public function setSwikEndDay($day) {
    	$this->swikEndDay = $day;
    	return ($this);
    }

    public function setSwikEndMonth($month) {
    	$this->swikEndMonth = $month;
    	return $this;
    }

    public function setSwikEndYear($year) {
    	$this->swikEndYear = $year;
    	return $this;
	}

    public function setSwikId($id) {
    	$this->swikId = $id;
    	return $this;
    }

    public function setCallbackUrl($url) {
    	$this->callbackUrl = $url;
    	return $this;
    }

	public function setSwikType($type) {
		$this->swikType = $type;
		return $this;
	}

    // All the getter

	public function getClientFirstName() {
		return $this->clientFirstName;
	}

    public function getClientLastName() {
    	return $this->clientLastName;
    }

    public function getClientPhoneNumber() {
    	return $this->clientPhoneNumber;
    }

    public function getClientEmail() {
    	return $this->clientEmail;
    }

    public function getClientLanguage() {
    	return $this->clientLanguage;
	}

    public function getSendEmail() {
    	return $this->sendEmail;
    }

    public function getSwikAmount() {
    	return $this->swikAmount;
    }

    public function getSwikDescription() {
    	return $this->swikDescription;
    }

    public function getSwikEndDay() {
    	return $this->swikEndDay;
    }

    public function getSwikEndMonth() {
    	return $this->swikEndMonth;
    }

    public function getSwikEndYear() {
    	return $this->swikEndYear;
	}

    public function getSwikId() {
    	return $this->swikId;
    }

    public function getCallbackUrl() {
    	return $this->callbackUrl;
    }

	public function getSwikType() {
		return $this->swikType;
	}

    public function getOptionnalParams() {
    	return ['email' => $this->sendEmail,
    			'business_pays' => $this->businessPays,
    			'last_name' => $this->clientLastName,
    			'first_name' => $this->clientFirstName,
				'phone_number' => $this->clientPhoneNumber,
				'swik_type' => $this->swikType
			   ];
	}
}

/**
 **
 ** The SwiklyAPI class let you call our API in an easy way.
 ** All the API routes are implemented and you can use them
 ** with only one function call.
 **
 */
class SwiklyAPI {

	private $apiKey;
	private $apiSecret;

	private $optFields;

	public function __construct($apiKey, $apiSecret, $env) {
		$this->apiKey = $apiKey;
		$this->apiSecret = $apiSecret;
		$env = strtolower($env);
		$this->url = "https://api.sandbox.swikly.com";
		if (strpos($env, 'prod') !== false) {
			$this->url = "https://api.swikly.com";
		}
		$this->optFields = ['email', 'business_pays', 'last_name', 'first_name', 'phone_number', 'swik_type'];
	}

	public function setApiKey($key) {
		$this->apiKey = $key;
		return $this;
	}

	public function setApiSecret($secret) {
		$this->apiSecret = $secret;
		return $this;
	}

	public function newSwik(\Swikly\Swik $swik) {
		// set required parameters
		$data = array (
			'swik_amount' => $swik->getSwikAmount(),
			'swik_description' => $swik->getSwikDescription(),
			'swik_end_day' => $swik->getSwikEndDay(),
			'swik_end_month' => $swik->getSwikEndMonth(),
			'swik_end_year' => $swik->getSwikEndYear(),
			'id' => $swik->getSwikId(),
			'client_email' => $swik->getClientEmail(),
			'swik_lang' => $swik->getClientLanguage(),
			'callback_url' => $swik->getCallbackUrl(),
		);

		// set optionnal parameters
		$optParams = $swik->getOptionnalParams();
		$len = count($this->optFields);
		for ($i = 0; $i < $len; $i++) {
			if (isset($optParams[$this->optFields[$i]])) {
				$data[$this->optFields[$i]] = $optParams[$this->optFields[$i]];
			}
		}

		$headerData = array(
			'Content-type: application/x-www-form-urlencoded',
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/newSwik');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$json = json_decode($result, true);

		return $json;
	}

	public function newDirectSwik(\Swikly\Swik $swik) {
		// set required parameters
		$data = array (
			'swik_amount' => $swik->getSwikAmount(),
			'swik_description' => $swik->getSwikDescription(),
			'swik_end_day' => $swik->getSwikEndDay(),
			'swik_end_month' => $swik->getSwikEndMonth(),
			'swik_end_year' => $swik->getSwikEndYear(),
			'id' => $swik->getSwikId(),
			'client_email' => $swik->getClientEmail(),
			'swik_lang' => $swik->getClientLanguage(),
			'callback_url' => $swik->getCallbackUrl(),
		);

		// set optionnal parameters
		$optParams = $swik->getOptionnalParams();
		$len = count($this->optFields);
		for ($i = 0; $i < $len; $i++) {
			if (isset($optParams[$this->optFields[$i]])) {
				$data[$this->optFields[$i]] = $optParams[$this->optFields[$i]];
			}
		}

		$headerData = array(
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/integratedSwik');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);

		$resp = curl_exec($ch);
		curl_close($ch);

		list($headers, $jsonResponse) = explode("\r\n\r\n", $resp, 2);
		preg_match_all('/^Location:(.*)$/mi', $headers, $matches);
		$result['redirect'] = !empty($matches[1]) ? trim($matches[1][0]) : '';

		$response = json_decode($jsonResponse, true);

		return is_array($response) ? array_merge($result, $response) : $result;
	}

	public function deleteSwik(\Swikly\Swik $swik) {
		// set required parameters
		$data = array (
			'id' => $swik->getSwikId()
		);

		// set optionnal parameter
		$email = $swik->getSendEmail();
		if ($email) {
			$data['email'] = $email;
		}

		$headerData = array(
			'Content-type: application/x-www-form-urlencoded',
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/cancelSwik');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$json = json_decode($result, true);

		return $json;
	}

	public function newPayment(\Swikly\Swik $swik) {
		// set required parameters
		$data = array (
			'swik_amount' => $swik->getSwikAmount(),
			'swik_description' => $swik->getSwikDescription(),
			'id' => $swik->getSwikId(),
			'client_email' => $swik->getClientEmail(),
			'swik_lang' => $swik->getClientLanguage(),
			'callback_url' => $swik->getCallbackUrl(),
		);

		// set optionnal parameters
		$optParams = $swik->getOptionnalParams();
		$len = count($this->optFields);
		for ($i = 0; $i < $len; $i++) {
			if (isset($optParams[$this->optFields[$i]])) {
				$data[$this->optFields[$i]] = $optParams[$this->optFields[$i]];
			}
		}

		$headerData = array(
			'Content-type: application/x-www-form-urlencoded',
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/newPayment');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);

		error_log("New payment result = " . serialize($result));

		$json = json_decode($result, true);

		return $json;
	}

	public function newDirectPayment(\Swikly\Swik $swik) {
		// set required parameters
		$data = array (
			'swik_amount' => $swik->getSwikAmount(),
			'swik_description' => $swik->getSwikDescription(),
			'id' => $swik->getSwikId(),
			'client_email' => $swik->getClientEmail(),
			'swik_lang' => $swik->getClientLanguage(),
			'callback_url' => $swik->getCallbackUrl(),
		);

		// set optionnal parameters
		$optParams = $swik->getOptionnalParams();
		$len = count($this->optFields);
		for ($i = 0; $i < $len; $i++) {
			if (isset($optParams[$this->optFields[$i]])) {
				$data[$this->optFields[$i]] = $optParams[$this->optFields[$i]];
			}
		}

		$headerData = array(
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$data = http_build_query($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/integratedPayment');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);

		$resp = curl_exec($ch);
		curl_close($ch);

		list($headers, $jsonResponse) = explode("\r\n\r\n", $resp, 2);
		preg_match_all('/^Location:(.*)$/mi', $headers, $matches);
		$result['redirect'] = !empty($matches[1]) ? trim($matches[1][0]) : '';

		$response = json_decode($jsonResponse, true);

		return is_array($response) ? array_merge($result, $response) : $result;
	}

	public function getListSwik() {
		$headerData = array(
			'Content-type: application/x-www-form-urlencoded',
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/listSwik');
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$json = json_decode($result, true);

		return $json;
	}

	public function getSwik($swik) {
		$headerData = array(
			'Content-type: application/x-www-form-urlencoded',
            "API_KEY: " . $this->apiKey,
			"API_SECRET: " . $this->apiSecret
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url . '/v1_0/getSwik?id=' . $swik->getSwikId());
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$json = json_decode($result, true);

		return $json;
	}

}
